//Objective 1 (insertOne)

db.room1.insertOne(
{
    "name": "single",
    "accommodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false
}
)

//Objective 2 (insertMany)

db.room.insertMany([
{"name": "double",
 "accommodates": 3,
 "price": 2000,
 "description": "A room fit for a small family going on a vacation",
 "rooms_available": 5,
 "isAvailable": false
 },
 
 {
 "name": "queen",
 "accommodates": 4,
 "price": 4000,
 "description": "A room with a queen sized bed perfect for a simple getaway",
 "rooms_available": 15,
 "isAvailable": false
 }
])

//Objective 3 (find)

db.room.find({"name": "double"})

//Objective 4 (updateOne)

db.room.updateOne({"_id": ObjectId("6419b4c543e1f1c2ecacdd9b") },
{$set: {"rooms_available": 0}}
)

//Objective 5

db.room.deleteMany({"rooms_available": 0})